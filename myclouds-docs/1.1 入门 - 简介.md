﻿## 概述
MyClouds是一个基于SpringBoot/Cloud生态和JavaEE技术栈的企业级微服务平台（微服务快速开发框架 & 微服务治理工具集）。支持研发团队快速交付高质量、可扩展的企业分布式微服务应用。

## 适用场景
MyClouds架构以互联网分布式应用开发优先并兼顾传统单体分层应用开发模式。因此，基于MyClouds丰富的组件体系，既能开发面向互联网的各类电商、金融、互联网+行业应用的PC端网站、管理系统、移动端H5应用、后台API接口服务等；也能快速开发面向传统软件行业的各类企业管理系统、各级各类政务信息化系统等。

## 前端技术选型
- [LayUI](https://www.layui.com/)

## 后端技术选型
- Spring/SpringCloud生态圈
- SpringBoot技术栈

## 缺省预置组件
- 通用资源权限管理系统（参考实现）

## 愿景规划
建一流企业级微服务平台，打造成熟稳定的技术组件生态和丰富的通用业务参考实现。

## 权限系统截屏
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151747_39cce02e_431745.jpeg "MyClouds企业级微服务开源平台-LOGIN.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151811_64047579_431745.jpeg "MyClouds企业级微服务开源平台-资源模块.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151853_b43471fb_431745.jpeg "MyClouds企业级微服务开源平台v2.1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151904_c10c726c_431745.png "MyClouds开发框架-用户管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151919_6d52d04c_431745.png "MyClouds开发框架-角色管理.png")


